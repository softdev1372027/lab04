/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.warathip.lab04;

/**
 *
 * @author win10
 */
public class Player {
    private char symbol;
    private int winCount,loseCount,drawCount;

        public Player(char symbol) {
        this.symbol = symbol;
    }
    public char getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }
    public void lose() {
        loseCount++;
    }
    public void draw() {
        drawCount++;
    }
}
